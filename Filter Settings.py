# import cv
from cv2 import *
import numpy as np


cap = VideoCapture(0)
capture = cv.CaptureFromCAM(-1)
num = 1


def f(a):
    1


namedWindow('level control')
createTrackbar('MAX_COL', 'level control', 0, 255, f)
createTrackbar('MIN_COL', 'level control', 0, 255, f)

createTrackbar('MAX_SAT', 'level control', 0, 255, f)
createTrackbar('MIN_SAT', 'level control', 0, 255, f)

createTrackbar('MAX_V', 'level control', 0, 255, f)
createTrackbar('MIN_V', 'level control', 0, 255, f)

while True:
    while not cap.isOpened():
        1

    _, frame = cap.read()
    imshow("Orig", frame)
    HSV = cvtColor(frame, COLOR_BGR2HSV)

    MAX_COL = getTrackbarPos('MAX_COL', 'level control')
    MIN_COL = getTrackbarPos('MIN_COL', 'level control')

    MAX_SAT = getTrackbarPos('MAX_SAT', 'level control')
    MIN_SAT = getTrackbarPos('MIN_SAT', 'level control')

    MAX_V = getTrackbarPos('MAX_V', 'level control')
    MIN_V = getTrackbarPos('MIN_V', 'level control')

    GREEN = inRange(HSV, np.array((MIN_COL, MIN_SAT, MIN_V)), np.array((MAX_COL, MAX_SAT, MAX_V)))
    GREEN = GaussianBlur(GREEN, (7, 7), 1.5)
    #GREEN = Canny(GREEN, 100, 200)

    #frame = GREEN + frame
    #frame = floodFill(frame,  GREEN, cv.RGB(255, 0, 0), cv.RGB(0, 255, 0))
    #frame = cv.Not(cv.fromarray(frame), cv.fromarray(GREEN))
    frame = bitwise_or(frame, frame, mask=GREEN)

    imshow("Green", frame)

    if cv.WaitKey(10) == 27:
        break
destroyAllWindows()