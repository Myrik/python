#import cv
import numpy as np
from cv2 import *
import vjoy, serial

USART = serial.Serial(port='COM9',timeout=0)

joyState = vjoy.JoystickState()
cap = VideoCapture(0)
num = 1

def GetBox(img):
    contours, hierarchy = findContours( img, RETR_EXTERNAL, CHAIN_APPROX_TC89_L1)
    if contours:
        contour = sorted(contours, key = contourArea, reverse = True)[0]
        a = contourArea(contour)
        if a<2000:
            return 0
        rect = minAreaRect(contour)
        return cv.BoxPoints(rect)
    else:
        return 0

def get_Contur(img, min, max):
    img = inRange(img, min, max)
    img = GaussianBlur(img, (5, 5), 1)
    _,img = threshold(img, 250, 255, THRESH_BINARY)
    img = dilate(img, None, iterations = 1)
    img = Canny(img, 100, 255)
    #img = GaussianBlur(img, (5, 5), 1)
    return img

def GetPoint(box):
    if not box:
        return 0,0
    point = [0,0]
    for pos in box:
        point[0] += pos[0]
        point[1] += pos[1]
    point[0] /= 4
    point[1] /= 4
    return point

while not cap.isOpened():
    1

while True:
    _, frame = cap.read()
    HSV = cvtColor(frame, COLOR_BGR2HSV)

    HSV = GaussianBlur(HSV, (5, 5), 1)

    RED = inRange(HSV, np.array((0, 180, 70)), np.array((5, 255, 255)))+ \
        inRange(HSV, np.array((170, 180, 70)), np.array((255, 255, 255)))
    # RED = GaussianBlur(RED, (5, 5), 1)
    RED = dilate(RED, None, iterations = 4)
    _,RED = threshold(RED, 200, 255, THRESH_BINARY)
    RED = Canny(RED, 100, 255)

    GREEN = get_Contur(HSV, np.array((60, 20, 30)), np.array((100, 255, 255)))

    imshow("RED", RED)
    imshow("GREEN", GREEN)

    Gbox = GetBox(GREEN)
    Rbox = GetBox(RED)
    Gpoint, Rpoint = (0,0),(0,0)

    if cv.WaitKey(10) == 27:
        break

    if Gbox and Rbox:
        Gpoint = GetPoint(Gbox)
        Rpoint = GetPoint(Rbox)

        circle(frame, (int(Rpoint[0]), int(Rpoint[1])), 2, (0, 255, 0), -1)
        circle(frame, (int(Gpoint[0]), int(Gpoint[1])), 2, (0, 0, 255), -1)
        line(frame,(int(Gpoint[0]),int(Gpoint[1])),(int(Rpoint[0]),int(Rpoint[1])),(255,0,0))

    imshow("Original", frame)
    if not Gbox or not Rbox:
        continue

    vjoy.Initialize()
    R = int(((Gpoint[0] - Rpoint[0])**2+(Gpoint[1]-Rpoint[1])**2)**0.5)
    S = 0
    symb = USART.read(1)
    if symb:
        symb = bytearray(symb)[0]
        for i in range(8):
            vjoy.SetButton(joyState,7-i,((symb>>i)&1))
        vjoy.UpdateJoyState(0, joyState)

    if R>20:
        S = (Gpoint[1]-Rpoint[1])/R
        POS = int(vjoy.AXIS_MAX*S)
        joyState.XAxis = POS
        vjoy.UpdateJoyState(0, joyState)

        print "R={0}, SIN={1:.3f} POS={2}".format(R,S,joyState.XAxis)
    vjoy.Shutdown()
    #box = np.int0(box)
    #drawContours(frame, [box], -1, (0, 0, 255), 1)

destroyAllWindows()